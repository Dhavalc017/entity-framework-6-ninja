﻿using NinjaDomain.Classes;
using NinjaDomain.DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //Database.SetInitializer(new NullDatabaseInitializer<NinjaContext>());
            // InsertNinja();
            //SimpleNinjaQueries();
            //QueryandUpdateNinja();
            //QueryForDisconnectedNinja();
            //RetrivalWithFind();
            //DeleteNinjaQuery();
            //InsertNinjaWithEquipment();
            //SimpleNinjaGraphQuery();
            ProjectionQuery();
        }

        private static void ProjectionQuery()
        {
            using (var context = new NinjaContext())
            {
                context.Database.Log = Console.WriteLine;
                var ninjas = context.Ninjas
                    .Select(n => new { n.Name, n.DateofBirth, n.EquipmentOwned })
                    .ToList();
            }
        }

        private static void SimpleNinjaGraphQuery()
        {
            using (var context = new NinjaContext())
            {
                context.Database.Log = Console.WriteLine;
                // var ninja = context.Ninjas.Include(n => n.EquipmentOwned)
                //.FirstOrDefault(n => n.Name.StartsWith("Kacy"));
                var ninja = context.Ninjas
                .FirstOrDefault(n => n.Name.StartsWith("Kacy"));
                Console.WriteLine("Ninja Retrived: {0}", ninja.Name );
                //to load the same ninja from the database
                //context.Entry(ninja).Collection(n => n.EquipmentOwned).Load();
                Console.WriteLine("Ninja Equipment Count : {0}",ninja.EquipmentOwned.Count());
            }
        }

        private static void InsertNinjaWithEquipment()
        {
            using (var context = new NinjaContext())
            {
                context.Database.Log = Console.WriteLine;
                var ninja = new Ninja
                {
                    Name = "Kacy Catanzaro",
                    ServedInOniwaban = false,
                    DateofBirth = new DateTime(1990, 1, 14),
                    ClanId = 1
                };
                var muscles = new NinjaEquipment
                {
                    Name = "Muscles",
                    Type = EquipmentType.Tool
                };

                var spunk = new NinjaEquipment
                {
                    Name = "Spunk",
                    Type = EquipmentType.Weapon
                };
                context.Ninjas.Add(ninja);
                ninja.EquipmentOwned.Add(muscles);
                ninja.EquipmentOwned.Add(spunk);
                context.SaveChanges();
            }
        }

        private static void DeleteNinjaQuery()
        {
            //Simple Call to delete the data 
            //using (var context  = new NinjaContext())
            //{
            //    context.Database.Log = Console.WriteLine;
            //    var ninja = context.Ninjas.FirstOrDefault();
            //    context.Ninjas.Remove(ninja);
            //    context.SaveChanges();

            //}

            //Deleteion in App
            Ninja ninja;
            using (var context = new NinjaContext())
            {
                context.Database.Log = Console.WriteLine; 
                ninja = context.Ninjas.FirstOrDefault();

            }
            using (var context = new NinjaContext()) {
                context.Database.Log = Console.WriteLine;
                context.Entry(ninja).State = EntityState.Deleted;
                context.Ninjas.Remove(ninja);
                context.SaveChanges();
            }
        }

        private static void RetrivalWithFind()
        {
            var key = 1;
            using (var context = new NinjaContext())
            {
                context.Database.Log = Console.WriteLine;
                var ninja = context.Ninjas.Find(key);
                Console.WriteLine("After Find #1 : {0}", ninja.Name);

                var someNinja = context.Ninjas.Find(key);
                Console.WriteLine("After Find #2 : {0}", someNinja.Name);
                ninja = null;

            }

        }

        private static void QueryForDisconnectedNinja()
        {
            Ninja ninja;
            using (var context = new NinjaContext()) {
                context.Database.Log = Console.WriteLine;
                ninja = context.Ninjas.FirstOrDefault();
            }
            ninja.ServedInOniwaban = (!ninja.ServedInOniwaban);
            using (var context = new NinjaContext()) {
                context.Database.Log = Console.WriteLine;
                context.Ninjas.Attach(ninja);
                context.Entry(ninja).State = EntityState.Modified;
                context.SaveChanges();

            }
        }

        private static void QueryandUpdateNinja()
        {
            using (var context = new NinjaContext())
            {
                context.Database.Log = Console.WriteLine;
                var ninja = context.Ninjas.FirstOrDefault();
                ninja.ServedInOniwaban = (!ninja.ServedInOniwaban);
                context.SaveChanges();

          

            }
        }

        private static void SimpleNinjaQueries()
        {
            using (var context = new NinjaContext()) {
                // var ninjas = context.Ninjas.ToList();
                var ninjas = context.Ninjas
                    .Where(
                    n => n.DateofBirth >= new DateTime(1994, 1, 1)
                    )
                    .OrderBy(n => n.Name)
                    .Skip(1)
                    .Take(1)
                    .FirstOrDefault();
                    //.FirstOrDefault();
                //var query = context.Ninjas;
                //var someninjas = query.ToList();
                /* foreach (var ninja in context.Ninjas)
                 * {
                 * Console.WriteLins(ninja.Name);
                 * }
                 */
               // foreach (var ninja in ninjas)
                //{
                    Console.WriteLine(ninjas.Name);
                //}

            }
        }

        private static void InsertNinja()
        {
            var ninja1 = new Ninja
            {
                Name = "Jahna",
                ServedInOniwaban = false,
                DateofBirth = new DateTime(1987, 02, 17),
                ClanId = 1
            };
            var ninja2 = new Ninja
            {
                Name = "Akshay",
                ServedInOniwaban = false,
                DateofBirth = new DateTime(1994, 12, 16),
                ClanId = 1
            };
            using (var context = new NinjaContext()) {
                context.Database.Log = Console.WriteLine;
                //For a single object to insert 
                //context.Ninjas.Add(ninja1);
                context.Ninjas.AddRange(new List<Ninja> { ninja1 , ninja2});
                context.SaveChanges();
            }
        }
    }
}
