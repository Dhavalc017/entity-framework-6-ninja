﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NinjaDomain.Classes
{
    public interface IModificationHistory
    {
        DateTime DateModified { set; get; }
        DateTime DateCreated { set; get; }
        bool IsDirty { get; set; }
    }
}
